'use strict';

angular.module('Appa',
		['shared', 'angular-growl', 'mgcrea.ngStrap', 'ui.router', 'ui.bootstrap' ]).config(
		[ '$httpProvider', function($httpProvider) {

		} ]).config([ 'growlProvider', function(growlProvider) {

	growlProvider.globalDisableIcons(false);
	growlProvider.onlyUniqueMessages(false);
	growlProvider.globalTimeToLive(3000);
	growlProvider.globalDisableCountDown(true);

} ]).config(function($logProvider) {

	$logProvider.debugEnabled(true);

});

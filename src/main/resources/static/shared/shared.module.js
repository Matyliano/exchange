angular.module('shared', []).directive('navbar', function(){
    return {
        templateUrl : 'shared/shared.navbar.template'
    };
}).directive('footer', function(){
    return {
        templateUrl : 'shared/shared.footer.template'
    };
});

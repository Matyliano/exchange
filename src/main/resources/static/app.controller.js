'use strict';

angular.module('Appa').controller(
		'AppCtrl',
		[ '$scope', '$rootScope', '$state', '$http',
				function($scope, $rootScope, $state, $http) {

					$scope.isErrorMessage = isErrorMessage;					

					function isErrorMessage(errorMessage) {
						
						if(errorMessage !== undefined) {
							return true;
						}
						
						return false;
					}					
			        
				} ]);